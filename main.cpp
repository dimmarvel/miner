#include "game/miner.hpp"
#include "game/setting.hpp"

int main(int argc, char* argv[])
{
    setting s(argc, argv);

    miner game(s);
    game.start();
    return 0;
}