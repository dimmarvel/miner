# Miner

## Getting started

```
./build.sh
```
# Game
--------------------------------
### Run game
```
./build/mine - Start game by default.
./build/mine 20 20 80 1 - Start game with parameters.
./build/mine <row-size> <col-size> <bomb-persentage> <is-cheats> - Parameters description.
```

`./build/mine`

![game_start.jpeg](./game_start.jpeg)

`./build/mine 20 20 80 1`

![start_with_param.jpeg](./start_with_param.jpeg)

### Move
```
Move:
'w' - top
'a' - left
's' - down
'd' - right
' ' (space) - activate cell
After input one of move char, you need press enter. 
```

![movement.jpeg](./movement.jpeg)


### Cheats
```
If you want activate cheats, you need input all parameters and the last one set by 1.
When cheats is active, after move, in "Current pos" info, you will see info about current cell.
vlue(BOMB) = here bomb;
vlue(activate) = cell already active;
vlue(hide) = cell doesnt active and have no bomb;
```
![cheats.jpeg](./cheats.jpeg)
