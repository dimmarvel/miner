#pragma once
#include <iostream>

class setting
{
public:
    setting(int argc, char* argv[]);

public:
    void show_setting();

public:
    uint32_t row_size;         //> 0 to uint16_t size 
    uint32_t col_size;         //> 0 to uint16_t size
    uint32_t bomb_percentage;  //> 1 to 99
    bool is_cheats;            //> true or false
};