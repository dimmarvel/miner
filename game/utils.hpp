#pragma once
#include <string>

enum STATUS : int32_t
{
    bomb_activated = '?',
    activate = 'a',
    bomb = 'b',

    hide = 'h',
};

inline std::string status_to_string(STATUS s)
{
    switch (s)
    {
    case STATUS::bomb: return "BOMB";
    case STATUS::bomb_activated: return "BOMB_ACTIVATED";
    case STATUS::hide: return "HIDE";
    case STATUS::activate: return "ACTIVATED";
    default:
        if(s>0)
            return "HIDE";
        else
            return "undefind";
    }
}


struct point 
{
    int row;
    int col;
};

struct cell
{
    int bomb_around;
    point _curr_pos;
    STATUS status;
};
