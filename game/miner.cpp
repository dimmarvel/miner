#include "miner.hpp"
#include <filesystem>
#include <fstream>
#include <thread>
#include <limits>

miner::miner(setting& s)
:  _map(s)
{}

void miner::start()
{
    char input;
    while (true)
    {
        _map.show();
        std::cout << ">";
        input = std::cin.get();

        std::cout.flush();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    
        _map.move(input);
        if(_map.is_lose())
        {
            _map.show();
            std::cout << "<-> .-. YOU LOSE .-. <->" << std::endl;
            break;
        }
        if(_map.is_win())
        {
            _map.show();
            std::cout << "YOU WIN" << std::endl;
            break;
        }
    }
}