#include "setting.hpp"
#include <string>
#include <vector>

setting::setting(int argc, char* argv[])
:   row_size(10),
    col_size(10),
    bomb_percentage(10),
    is_cheats(false)
{
    if (argc != 5)
    {
        std::cout << "==================Game Mode==================" << std::endl;
        std::cout << "Should be 4 parameters, game start by default settings." << std::endl;
        std::cout << "Template: ./mine <row-size> <col-size> <bomb-persentage> <is-cheats>"  << std::endl;
        std::cout << "Example:  ./mine 10 10 10 0" << std::endl;
        std::cout << "=============================================" << std::endl;
    } else
    {
        std::vector<std::string> params;
        for(int i = 1; i < argc; ++i)
            params.push_back(argv[i]);

        row_size = std::stoi(params[0]);
        col_size = std::stoi(params[1]);
        bomb_percentage = std::stoi(params[2]);
        is_cheats = std::stoi(params[3]);
    }
    show_setting();   
}

void setting::show_setting()
{
    std::cout << "-----Game settings-----" << std::endl;
    std::cout << "- Row size: " << std::to_string(row_size) << std::endl;
    std::cout << "- Col size: " << std::to_string(row_size) << std::endl;
    std::cout << "- Bomb percentage: " << std::to_string(bomb_percentage) << "%" << std::endl;
    std::cout << "- Cheats is : " << ((is_cheats) ? "ON" : "OFF") << std::endl;
    std::cout << "-----------------------" << std::endl;
}