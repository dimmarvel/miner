#include "map.hpp"
#include <random>
#include <iomanip>

map::map(const setting& s) 
:   _table(),
    _current_pos({0,0}),
    _settings(s),
    _was_fisrst_activate(false),
    _bomb_amount(0),
    _is_activated_amount(0),
    _table_size(0)
{
    for (int i = 0; i < _settings.row_size; i++)
    {
        std::vector<cell> vec;
        for (int j = 0; j < _settings.col_size; j++)
            vec.emplace_back(cell{0,point{i,j}, STATUS::hide});
            
        _table.push_back(vec);
    }
    _table_size = _settings.row_size * _settings.col_size;
}

void map::show(bool hide) const
{
    std::cout << "Table: " << std::endl;
    for (size_t row = 0; row < _table.size(); row++)
    {
        for (size_t col = 0; col < _table[row].size(); col++)
        {
            bool is_current = (_current_pos.row == row && _current_pos.col == col);

            if(is_current)
                std::cout << "[";
            else
                std::cout << " ";
            
            if(hide &&  _table[row][col].status != STATUS::activate && 
                        _table[row][col].status != STATUS::bomb_activated)
                std::cout << ".";
            else if(_table[row][col].status == STATUS::bomb_activated)
                std::cout << (char)_table[row][col].status;
            else if(_table[row][col].status == STATUS::activate)
                std::cout << _table[row][col].bomb_around;
            else
                std::cout <<_table[row][col].bomb_around;

            if(is_current)
                std::cout << "]";
            else
                std::cout <<  " ";
        }
        std::cout << std::endl;
    }
}

void map::move(char ch)
{    
    std::cout << "Move: ";
    switch(ch)
    {
        case 'w':
        { 
            if((_current_pos.row - 1) >= 0) _current_pos.row--; 
            std::cout << "top" << std::endl; 
            break;
        }
        case 'a':
        { 
            if((_current_pos.col - 1) >= 0) _current_pos.col--; 
            std::cout << "left" << std::endl; 
            break;
        }
        case 's':
        { 
            if((_current_pos.row + 1) < _table[_current_pos.row].size()) _current_pos.row++;
            std::cout << "down" << std::endl; 
            break;
        }
        case 'd':
        { 
            if((_current_pos.col + 1) < _table.size()) _current_pos.col++; 
            std::cout << "right" << std::endl; 
            break;
        }
        case ' ':
        { 
            if(!_was_fisrst_activate)
            {
                generate_map(_table.size(), _table[0].size());
                _was_fisrst_activate = true;
            }
            
            if(_table[_current_pos.row][_current_pos.col].status == STATUS::bomb)
            {
                _table[_current_pos.row][_current_pos.col].status = STATUS::bomb_activated;
                break;
            }

            if(_table[_current_pos.row][_current_pos.col].status != STATUS::activate)
            {
                open_around(point{_current_pos.row, _current_pos.col});
                _table[_current_pos.row][_current_pos.col].status = STATUS::activate;
            }

            recalc_activated();

            std::cout << "activate" << std::endl; 
            break;
        }
        default:
            std::cout << "Undefind move" << std::endl;
        break;
    }
    info();
}

void map::info()
{
    std::cout << "Current pos: "    << _current_pos.row << ":" 
                                    << _current_pos.col;
    if(_settings.is_cheats)
        std::cout << ", vlue("  << status_to_string(_table[_current_pos.row][_current_pos.col].status) 
                                << ")" << std::endl;
    else
        std::cout << std::endl;

    std::cout << "Bomb amount: "    << _bomb_amount << std::endl;
    std::cout << "Amount\\active: "    << _table_size << "\\" << _is_activated_amount << std::endl;
}

bool map::is_lose()
{
    return _table[_current_pos.row][_current_pos.col].status == STATUS::bomb_activated;
}

bool map::generate_mines(size_t from, size_t to)
{
    std::random_device dev;
    std::mt19937 rng(dev());
    std::uniform_int_distribution<std::mt19937::result_type> dist(from, to);
    auto rand = dist(rng);
    if(rand >= 1 && rand <= _settings.bomb_percentage)
    {
        _bomb_amount++;
        return true;
    }
    return false;
}

void map::generate_map(size_t row, size_t col)
{
    for (int i = 0; i < row; i++)
    {
        for (int j = 0; j < col; j++)
        {
            if(i == _current_pos.row && j == _current_pos.col)
                _table[i][j] = cell{0, point{i,j}, STATUS::hide};
            else if(generate_mines(1, 100))
                    _table[i][j] = cell{0, point{i,j}, STATUS::bomb};
            else
                _table[i][j] = cell{0, point{i,j}, STATUS::hide};
        }
    }
    generate_right_map();
}

void map::generate_right_map()
{
    for (int i = 0; i < _table.size(); i++)
        for (int j = 0; j < _table[i].size(); j++)
            if(_table[i][j].status == STATUS::bomb)
                increase_bomb_count_around({i,j});
}

void map::change_cell(cell c)
{
    _table[c._curr_pos.row][c._curr_pos.col] = c;
}

void map::increase_bomb_count_around(point p)
{
    for (int i = p.row - 1; i <= p.row  + 1; ++i)
        for (int j = p.col - 1; j <= p.col + 1; ++j)
            if (0 <= i && i < _table.size() && 0 <= j && j < _table[0].size() && (i != p.row || j != p.col))
                if(_table[i][j].status != STATUS::bomb)
                    _table[i][j].bomb_around++;
}

void map::open_around(point p)
{
    for (int i = p.row - 1; i <= p.row  + 1; ++i)
        for (int j = p.col - 1; j <= p.col + 1; ++j)
            if (0 <= i && i < _table.size() && 0 <= j && j < _table[0].size() && (i != p.row || j != p.col))
                if(_table[i][j].status == STATUS::hide)
                {
                    _table[i][j].status = STATUS::activate;
                    if(_table[i][j].bomb_around == 0)
                        open_around(point{i,j});
                }
}

bool map::is_win()
{
    return (_table_size - _bomb_amount) == _is_activated_amount;
}

void map::recalc_activated()
{
    _is_activated_amount = 0;
    for (size_t i = 0; i < _table.size(); i++)
        for (size_t j = 0; j < _table[i].size(); j++)
            if(_table[i][j].status == STATUS::activate)
                _is_activated_amount++;
}