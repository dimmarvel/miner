#pragma once
#include <iostream>
#include <vector>
#include "utils.hpp"
#include "setting.hpp"

class map
{
    std::vector<std::vector<cell>> _table;
    point _current_pos;  
    bool  _was_fisrst_activate;
    uint16_t _bomb_amount;
    uint16_t _is_activated_amount;
    uint16_t _table_size;

    const setting& _settings;  
    
public:
    map(const setting& s);

public:
    void show(bool hide = true) const;
    void move(char ch);
    
    bool is_lose();
    bool is_win();

private:
    bool generate_mines(size_t from, size_t to);
    void generate_map(size_t row, size_t col);
    void change_cell(cell c);
    void generate_right_map();
    void increase_bomb_count_around(point p);
    void open_around(point p);
    void recalc_activated();
    void info();
};